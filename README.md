# Python3-programming-bible

About this course
Python 3 is one of the most popular programming languages. Companies like Facebook, Microsoft and Apple all want Python.

Course content

### Section 01: Introduction & Setup
### Section 02: Basics
### Section 03: Classes/Objects
### Section 04: Regular Expressions
### Section 05: CGI Programming
### Section 06: Database
### Section 07: Multithreading
### Section 08: XML
### Section 09: GUI