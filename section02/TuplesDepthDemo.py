tuple1 = (1,2,3,4,5,"a","b","c")


# Creating Tuples
print(tuple1)
print(tuple1[1])
print(tuple1[0:4])

# Tuple operators
print(len(tuple1))

# Deleting tuples
del tuple1


