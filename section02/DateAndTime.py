import time
import calendar


# Ticks
var1 = time.time()
print(var1)

# Local time
local = time.localtime(time.time())
print(local)

# Formatted time
timeLocal = time.asctime(time.localtime(time.time()))
print(timeLocal)

#calendar
calendarvar = calendar.month(2018,8)
print(calendarvar)