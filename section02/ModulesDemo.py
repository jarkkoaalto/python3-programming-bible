# Cook your dish here

#Accessing /Including a module
import math

# Specific module functionality imported
# ftom math import pi
# print(pi)
print(math.pi)

contains = dir(math)
print(contains)