dic = {}
dic[1]  = "Item 1"
dic[2] = "Python2"
dic[3] = "Python3"

print(dic[1])
print(dic[2])
print(dic[3])
print(dic.values())
print(dic.keys())

newDic = {1:"koe",2:"test",3:"dic"}
print(newDic)