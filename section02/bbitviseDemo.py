var1 = 13       # 1101
var2 = 5        # 0101
#               -------
# AND             0101

print(var1 & var2)

# 13        1101
#  5        0101
#           -----
#           1101
print(var1 | var2)

# 13        1101
#  5        0101
#           ----
# XOR       1000

print(var1 * var2)

# 13                1101
# ones conement =   0010
print(-var1)

# Binary sift left
print(var1 << 1)

# Binar sift right
print(var1 >> 1)
