#Assert statements

def Function1(var1):
    assert var1 != 0, " Zero in invalid"
    return 10 / var1


print(Function1(4))

try:
    file = open("file.txt","w")
    file.write("Jellona")
except IOError:
    print("file not found")
else:
    print("A OK")
    print()

    file.close()