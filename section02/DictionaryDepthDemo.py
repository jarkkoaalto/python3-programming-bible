dictionary1 = {'a':1, 'b':2, 'c':3, 'd':4}

#Accessing Value
print(dictionary1['a'])
print(dictionary1['b'])
print(dictionary1['c'])

# Updateing value
dictionary1['a'] = "Hello world"
print(dictionary1['a'])


# Deleing values
del dictionary1['b']
print(dictionary1)

# deleting entire
#dictionary1.clear()
#del dictionary1

print(len(dictionary1))
