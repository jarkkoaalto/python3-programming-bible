string1 = "Pyhton is amazing"
print(string1)
print(string1[1])
print(string1[1:4])

print(string1[:4] + "Bob")

string2 = "Python is the best"

print(string2)

print("Hello\nWorld")

string3 = string1 + " " + string2
print(string3)

var1 = "hello"
var2 = "world"

print(" %s This is the best %s s" % (var1, var2))


lorem = """
Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
Mauris posuere orci arcu, efficitur finibus augue placerat tempus.
Donec quis volutpat eros. 
Sed ornare mauris placerat sem tincidunt rutrum. 
Vestibulum tempor eget velit sed lacinia. 
Suspendisse nulla ipsum, lobortis sit amet massa id, posuere tristique orci. 
Quisque tincidunt suscipit eros sed elementum. 
Etiam sed tellus sodales, rhoncus risus ac, elementum metus. 
Integer et ipsum dolor. Donec venenatis, sapien vitae tincidunt consectetur, 
magna augue condimentum purus, non commodo dolor dui tempor turpis. 
Nunc eget augue velit. In rutrum metus orci, sodales eleifend purus elementum ut.
"""
print(lorem)
var4 = "hello world"

print(var4.capitalize())