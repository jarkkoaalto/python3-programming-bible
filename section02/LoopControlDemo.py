var1 = "Hello world"

for char in var1:
    if(char == ' '):
        print("There was space, oh no")
        break
    print(char)

for char in var1:
    if(char == ' '):
        print("There was space")
        continue
    print(char)

for char in var1:
    if char == ' ':
        pass
        print("Passing world")
    print(char)