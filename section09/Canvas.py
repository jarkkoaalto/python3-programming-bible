import tkinter
from tkinter import messagebox
from nltk.draw.util import CanvasWidget

window = tkinter.Tk()

CanvasWidget = tkinter.Canvas(window, bg = "red", height=512, width = 512)
coord = 10, 50, 512, 512
arcObject = CanvasWidget.create_arc(coord, start = 0, extent= 270, fill ="white")

lineObject = CanvasWidget.create_line(75,10,20,150,fill="Black")

CanvasWidget.pack()
window.mainloop()